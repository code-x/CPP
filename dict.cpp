/*
调用第三方库cJson
参考链接https://www.cnblogs.com/liunianshiwei/p/6087596.html
CJson下载地址https://github.com/DaveGamble/cJSON
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<cJSON.h>

#include"dict.h"

Dict::Dict() {}

Dict::Dict(char*char_json) {
	this->json = cJSON_Parse(char_json);
}

Dict::~Dict() {
	cJSON_Delete(this->json);
}

int Dict::set(char* key, char* value) {
	cJSON_AddItemToObject(this->json, key, cJSON_CreateString(value));
}

int Dict::set(char* key, int value) {
	cJSON_AddNumberToObject(this->json, key, value);
}

int Dict::load(char* filename) {
	FILE *fp = NULL;
	char *buff = NULL;
	fp = fopen(filename, "r");
	if (fp == NULL) {
		printf("load file %s error\n", filename);
		return -1;
	}

	fseek(fp, 0, SEEK_END);
	long lsize = ftell(fp);

	buff = (char*)malloc(lsize + 1);
	rewind(fp);
	fread(buff, sizeof(char), lsize, fp);
	buff[lsize] = '\0';

	this->json = cJSON_Parse(buff);

	fclose(fp);
	return 0;
}

char* Dict::get(char* key) {
	cJSON *node = NULL;
	node = cJSON_GetObjectItem(this->json, key);
	if (node == NULL)return NULL;

	return node->valuestring;
}

void k0_test() {
	//从缓冲区中解析出JSON结构
	Dict dict;
	dict.load("exec.json");
	//将传入的JSON结构转化为字符串 并打印
	char *buf = NULL;
	int flag;
	buf = dict.get("habit");
	printf("flag:%d,data:%s\n", flag, buf);
	//打开一个exec.json文件，并写入json内容
	//FILE *fp = fopen("exec.json", "w");
	//fwrite(buf, strlen(buf), 1, fp);

	//fclose(fp);
	free(buf);
	return;

}