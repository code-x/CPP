#pragma once
#include<cJSON.h>
class Dict {
public:
	Dict();
	Dict(char *);
	~Dict();
	int set(char*, char*);
	int set(char*, int);
	char* get(char*);
	int load(char*);
private:
	cJSON *json;
};